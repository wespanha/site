<?php

class Plugins_Auth_AuthSetup
{

    public static function login($email, $password)
    {
        $db = Zend_Registry::get('db');

        $adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName('user')
                ->setIdentityColumn('email')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('sha1(?)');

        $adapter->getDbSelect()->where("status = 'y'");

        $adapter->setIdentity($email)
                ->setCredential($password);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);

        $user = new Plugins_Auth_UserAcl();

        if ($result->isValid())
        {
            $data = $adapter->getResultRowObject(NULL, 'password');
            $user->setName($data->name);
            $user->setEmail($data->email);
            $user->setRole($data->role);
            $auth->getStorage()->write($user);
            return true;
        }else
            return false;
    }

}