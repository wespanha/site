<?php

class Panel_UserController extends Zend_Controller_Action
{

	/**
	* Armazena a instancia model usuário
	* @access private
	*/
	private $model_user;

    private $form_user;

    public function init() {
    	//instacia o model usuário
    	$this->model_user = new Panel_Model_User();
        $this->form_user = new Panel_Form_User();
    	
    }

    public function indexAction() {
        //lista na view os usuarios
        $this->view->assign('dataAuth', Zend_Auth::getInstance()->getStorage()->read());
    	$this->view->assign('users', $this->model_user->findAll());
		
    }

    public function createAction() {

        if( $this->getRequest()->isPost() ) {
            if( $this->form_user->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_user->setName($this->form_user->getValue('name'));
                $this->model_user->setEmail($this->form_user->getValue('email'));
                $this->model_user->setPassword($this->form_user->getValue('password'));
                $this->model_user->setRole($this->form_user->getValue('role'));
                $this->model_user->setStatus($this->form_user->getValue('status'));

                if( $this->model_user->create() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Usuário criado com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível criar o usuário. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'user', 'panel');
            }
        }

        $this->view->assign('form', $this->form_user);

    }

    public function updateAction() {

        if( $this->getRequest()->isPost() ) {
            if( $this->form_user->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_user->setId($this->form_user->getValue('id'));
                $this->model_user->setName($this->form_user->getValue('name'));
                $this->model_user->setEmail($this->form_user->getValue('email'));
                $this->model_user->setPassword($this->form_user->getValue('password'));
                $this->model_user->setRole($this->form_user->getValue('role'));
                $this->model_user->setStatus($this->form_user->getValue('status'));

                if( $this->model_user->update() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Usuário editado com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível editar o usuário. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'user', 'panel');
            }
        }

        $this->model_user->find($this->getParam('id'));

        $data = [
            'id' => $this->model_user->getId(),
            'name' => $this->model_user->getName(),
            'email' => $this->model_user->getEmail(),
            'role' => $this->model_user->getRole(),
            'status' => $this->model_user->getStatus(),
        ];

        $this->form_user->populate($data);
        $this->view->assign('form', $this->form_user);

    }

    public function deleteAction() {

        if( $this->model_user->delete($this->getParam('id')) ){
            $this->view->assign('status', true);
            $this->view->assign('message', 'Usuário excluído com sucesso!');
        } else {
            $this->view->assign('status', false);
            $this->view->assign('message', 'Não foi possível excluir o usuário. Favor tente novamente ou entre em contato com o administrador.');
        }

        $this->_forward('index', 'user', 'panel');

    }

}
