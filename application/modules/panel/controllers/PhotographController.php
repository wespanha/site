<?php

class Panel_PhotographController extends Zend_Controller_Action
{

	/**
	* Armazena a instancia model usuário
	* @access private
	*/
	private $model_photograph;

    private $model_event;

    private $form_photograph;

    public function init() {
    	//instacia o model da galeria de fotos
    	$this->model_photograph = new Panel_Model_Photograph();
        $this->model_event = new Panel_Model_Event();
        $this->form_photograph = new Panel_Form_Photograph($this->model_event->findAll());
    	
    }

    public function indexAction() {
    	
        $this->view->assign('photographs', $this->model_photograph->findAll());
		
    }

    public function createAction() {

        if( $this->getRequest()->isPost() ) {
            if( $this->form_photograph->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_photograph->setName($this->form_photograph->getValue('name'));
                $this->model_photograph->setDescription($this->form_photograph->getValue('description'));
                $this->model_photograph->setIdEvent($this->form_photograph->getValue('idEvent'));
                $this->model_photograph->setIdAlbumFacebook($this->form_photograph->getValue('idAlbumFacebook'));
                $this->model_photograph->setImgAlbum($this->form_photograph->getValue('imgAlbum'));
                $this->model_photograph->setStatus($this->form_photograph->getValue('status'));

                if( $this->model_photograph->create() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Galeria de fotos criada com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível criar a galeria de fotos. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'photograph', 'panel');
            }
        }

        $this->view->assign('form', $this->form_photograph);

    }

    public function updateAction() {

        if( $this->getRequest()->isPost() ) {
            if( $this->form_photograph->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_photograph->setId($this->form_photograph->getValue('id'));
                $this->model_photograph->setName($this->form_photograph->getValue('name'));
                $this->model_photograph->setDescription($this->form_photograph->getValue('description'));
                $this->model_photograph->setIdEvent($this->form_photograph->getValue('idEvent'));
                $this->model_photograph->setIdAlbumFacebook($this->form_photograph->getValue('idAlbumFacebook'));
                $this->model_photograph->setImgAlbum($this->form_photograph->getValue('imgAlbum'));
                $this->model_photograph->setStatus($this->form_photograph->getValue('status'));

                if( $this->model_photograph->update() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Galeria de fotos editada com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível editar a galeria de fotos. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'photograph', 'panel');
            }
        }

        $this->model_photograph->find($this->getParam('id'));
        $data = [
            'id' => $this->model_photograph->getId(),
            'name' => $this->model_photograph->getName(),
            'description' => $this->model_photograph->getDescription(),
            'idEvent' => $this->model_photograph->getIdEvent(),
            'idAlbumFacebook' => $this->model_photograph->getIdAlbumFacebook(),
            'imgAlbum' => $this->model_photograph->getImgAlbum(),
            'status' => $this->model_photograph->getStatus()
        ];

        $this->form_photograph->populate($data);
        $this->view->assign('form', $this->form_photograph);

    }

    public function deleteAction() {

        if( $this->model_photograph->delete($this->getParam('id')) ){
            $this->view->assign('status', true);
            $this->view->assign('message', 'Galeria de fotos excluída com sucesso!');
        } else {
            $this->view->assign('status', false);
            $this->view->assign('message', 'Não foi possível excluir a galeria de fotos. Favor tente novamente ou entre em contato com o administrador.');
        }

        $this->_forward('index', 'photograph', 'panel');

    }

}
