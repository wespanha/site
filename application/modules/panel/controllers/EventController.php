<?php

class Panel_EventController extends Zend_Controller_Action
{

	/**
	* Armazena a instancia model usuário
	* @access private
	*/
	private $model_event;

    private $form_event;
    private $form_eventcape;
    private $form_eventsponsor;
    private $form_eventrestaurant;
    private $form_restaurantdetail;

    public function init() {
    	//instacia o model usuário
    	$this->model_event = new Panel_Model_Event();
        $this->form_event = new Panel_Form_Event();
        $this->form_eventcape = new Panel_Form_Eventcape();
        $this->form_eventrestaurant = new Panel_Form_Eventrestaurant();
        $this->form_restaurantdetail = new Panel_Form_Restaurantdetail();
    	
    }

    public function indexAction() {
    	
        $this->view->assign('events', $this->model_event->findAll());
		
    }

    public function createAction() {

        if( $this->getRequest()->isPost() ) {

            if( $this->form_event->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_event->setType($this->form_event->getValue('type'));
                $this->model_event->setYear($this->form_event->getValue('year'));
                $this->model_event->setRegulation($this->form_event->getValue('regulation'));
                $this->model_event->setImage($this->form_event->getValue('image'));

                if( $this->model_event->create() ){

                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Evento criado com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível criar o evento. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }
        }

        $this->view->assign('form', $this->form_event);

    }

    public function updateAction() {

        if( $this->getRequest()->isPost() ) {

            if( $this->form_event->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_event->setId($this->form_event->getValue('id'));
                $this->model_event->setType($this->form_event->getValue('type'));
                $this->model_event->setYear($this->form_event->getValue('year'));
                $this->model_event->setRegulation($this->form_event->getValue('regulation'));

                if( $this->model_event->update() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Evento editado com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível editar o evento. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }
        }

        $this->model_event->find($this->getParam('id'));

        $data = [
            'id' => $this->model_event->getId(),
            'type' => $this->model_event->getType(),
            'year' => $this->model_event->getYear(),
            'regulation' => $this->model_event->getRegulation()
        ];

        $this->form_event->populate($data);
        $this->view->assign('form', $this->form_event);

    }

    public function deleteAction() {

        if( $this->model_event->delete($this->getParam('id')) ){
            $this->view->assign('status', true);
            $this->view->assign('message', 'Evento excluído com sucesso!');
        } else {
            $this->view->assign('status', false);
            $this->view->assign('message', 'Não foi possível excluir o evento. Favor tente novamente ou entre em contato com o administrador.');
        }

        $this->_forward('index', 'event', 'panel');

    }

    public function capeAction() {

        //metodo para atualizar a foto de capa
        if( $this->getRequest()->isPost() ) {

            if( $this->form_eventcape->isValid($this->getRequest()->getPost()) ){
                //seta os dados do usuário para o model
                $this->model_event->setId($this->form_eventcape->getValue('id'));
                $this->model_event->setImage($this->form_eventcape->getValue('image'));

                if( $this->model_event->updateCape() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Foto de capa editada com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível editar a foto de capa. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }
        }

        //metodo para excluir a foto de capa
        if( $this->getParam('type') == 'delete' ) {
            $this->model_event->setId($this->getParam('id'));

            if( $this->model_event->deleteCape() ){
                $this->view->assign('status', true);
                $this->view->assign('message', 'Foto de capa exluida com sucesso!');
            } else {
                $this->view->assign('status', false);
                $this->view->assign('message', 'Não foi possível excluir a foto de capa. Favor tente novamente ou entre em contato com o administrador.');
            }

            $this->_forward('index', 'event', 'panel');
        }

        //busca o evento e popula o formulário
        $this->model_event->find($this->getParam('id'));

        $data = [
            'id' => $this->model_event->getId(),
            'image' => $this->model_event->getImage()
        ];

        $this->form_eventcape->populate($data);

        //assina na view os dados do evento e o formulário
        $this->view->assign('event', $data);
        $this->view->assign('form', $this->form_eventcape);

    }

    public function sponsorAction() {

        //busca o evento e popula o formulário
        $this->model_event->find($this->getParam('id'));

        $this->form_eventsponsor = new Panel_Form_Eventsponsor($this->model_event->getType(), $this->model_event->getYear());
        $this->view->assign('imgSponsor', $this->model_event->findImageSponsor());

        $data = [
            'id' => $this->model_event->getId(),
            'image' => $this->model_event->getSponsor(),
            'image_responsive' => $this->model_event->getSponsorResponsive()
        ];

        $this->form_eventsponsor->populate($data);


        //assina na view os dados do evento e o formulário
        $this->view->assign('event', $data);
        $this->view->assign('form', $this->form_eventsponsor);

        //metodo para atualizar a foto de capa
        if( $this->getRequest()->isPost() ) {

            if( $this->form_eventsponsor->isValid($this->getRequest()->getPost()) ){
                
                $this->model_event->setId($this->form_eventsponsor->getValue('id'));
                $this->model_event->setSponsor($this->form_eventsponsor->getValue('image'));
                $this->model_event->setSponsorResponsive($this->form_eventsponsor->getValue('image_responsive'));

                if( $this->model_event->uploadSponsor() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Imagem de patrocinadores cadastrada com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível cadastrar a imagem de patrocinadores. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }
        }
        //metodo para excluir a foto de capa
        if( $this->getParam('type') == 'delete' && $this->getParam('typeImg') ) {
            $this->model_event->setId($this->getParam('id'));

            if( $this->model_event->deleteSponsor( $this->getParam('typeImg') ) ){
                $this->view->assign('status', true);
                $this->view->assign('message', 'Patrocinadores excluidos com sucesso!');
            } else {
                $this->view->assign('status', false);
                $this->view->assign('message', 'Não foi possível excluir os patrocinadores. Favor tente novamente ou entre em contato com o administrador.');
            }

            $this->_forward('index', 'event', 'panel');

        }

    }

    public function restaurantAction() {

        //metodo para atualizar a foto de capa
        if( $this->getRequest()->isPost() ) {
            
            if( $this->form_eventrestaurant->isValid($this->getRequest()->getPost()) ){

                $this->model_event->setId($this->form_eventrestaurant->getValue('id'));
                $this->model_event->setYear($this->form_eventrestaurant->getValue('year'));
                $this->model_event->setType($this->form_eventrestaurant->getValue('type'));

                if( $this->model_event->uploadRestaurant() ){
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Imagens cadastradas com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível cadastrar as imagens do guia. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }
        }

        //metodo para excluir a foto de capa
        if( $this->getParam('type') == 'delete' ) {

            if( $this->model_event->deleteRestaurant( $this->getParam('id') ) ){
                $this->view->assign('status', true);
                $this->view->assign('message', 'Guia excluido com sucesso!');
            } else {
                $this->view->assign('status', false);
                $this->view->assign('message', 'Não foi possível excluir o guia. Favor tente novamente ou entre em contato com o administrador.');
            }

            $this->_forward('index', 'event', 'panel');

        //metodo para listagem dos patrocinadores
        } else {

            //busca o evento e popula o formulário
            $this->model_event->find($this->getParam('id'));
            $this->view->assign('imgRestaurant', $this->model_event->findImageRestaurant());

            $data = [
                'id' => $this->model_event->getId(),
                'year' => $this->model_event->getYear(),
                'type' => $this->model_event->getType(),
                'image' => $this->model_event->getImage()
            ];

            $this->form_eventrestaurant->populate($data);

            //assina na view os dados do evento e o formulário
            $this->view->assign('event', $data);
            $this->view->assign('form', $this->form_eventrestaurant);

        }

    }

    public function detailrestaurantAction() {

        if( $this->getRequest()->isPost() ) {
            if( $this->form_restaurantdetail->isValid( $this->getRequest()->getPost() ) ) {
                
                if( $this->model_event->updateRestaurant($this->form_restaurantdetail->getValues()) ) {
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Informações do restaurante cadastradas com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível cadastrar as informações do restaurante. Favor tente novamente ou entre em contato com o administrador.');
                }

                $this->_forward('index', 'event', 'panel');
            }

        }

        //verifica se passou o parametro do restaurante
        if( $this->getParam('id') ) {
            //busca os dados do restaurante para popular o formulário
            $restaurant = $this->model_event->findRestaurant($this->getParam('id'));
            //monta os dados para popular no formulario
            $data = [
                'id' => $restaurant['id'],
                'name' => $restaurant['name'],
                'latitude' => $restaurant['latitude'],
                'longitude' => $restaurant['longitude']
            ];

            $this->form_restaurantdetail->populate($data);
            //assina para view a imagem do restaurante
            $this->view->assign('imgRestaurant', $restaurant['image']);
        }
        //assina para view o formulário para preecher os dados
        $this->view->assign('form', $this->form_restaurantdetail);
        
    }

}
