<?php

class Panel_Model_User 
{

	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	/**
	* Armazena o id do usuário
	* @name id
	* @access private
	*/
	private $id;
	
	/**
	* Armazena o nome do usuário
	* @name name
	* @access private
	*/
	private $name;

	/**
	* Armazena o email do usuário
	* @name email
	* @access private
	*/
	private $email;

	/**
	* Armazena a senha de acesso do usuário
	* @name password
	* @access private
	*/
	private $password;

	/**
	* Armazena o status do usuário no painel
	* @name status
	* @access private
	*/
	private $status;

	/**
	* Armazena a permissão do usuário no painel
	* @name role
	* @access private
	*/
	private $role;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Método para buscar um usuário especifico
	* @name find()
	* @param $id [id do usuário]
	* @return bool
	*/
	public function find( $id = null, $email = null ) {

		$sql = $this->db->select()
					    ->from('user', array('id','name','email','role','status'));

		if( !is_null($id) )
			$sql = $sql->where('id = ?', $id);

		if( !is_null($email) )
			$sql = $sql->where('email = ?', $email);
		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			$this->id = $result['id'];
			$this->name = $result['name'];
			$this->email = $result['email'];
			$this->role = $result['role'];
			$this->status = $result['status'];

			return true;
		}

		return false;
	}

	/**
	* Método para buscar todos os usuários
	* @name findAll()
	* @param $status [filtrar por usuários ativos ou desativados (y, n)]
	* @return array
	*/
	public function findAll( $status = null ) {
		
		$sql = $this->db->select()
					    ->from('user', array('id','name','email','role','status'))
					    ->order('name');

		if( !is_null($status) )
			$sql = $sql->where('status = ?', $status);
		
		$result = $this->db->fetchAll($sql);
		return ( $result ) ? $result : false;

	}

	/**
	* Método para criar usuário
	* @name create()
	* @return bool
	*/
	public function create() {

		if( !$this->find(null, $this->email) && $this->password != '' ) {
			$data = [
				'name' => $this->name,
				'email' => $this->email,
				'password' => sha1($this->password),
				'role' => $this->role,
				'status' => $this->status
			];

			if( $this->db->insert('user', $data) )
				return true;
		}

		return false;
	}

	/**
	* Método para deletar o usuário
	* @name delete()
	* @return bool
	*/
	public function delete($id) {

		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('user', $where) )
			return true;

		return false;
	}

	/**
	* Método para atualizar os registro do usuário
	* @name update()
	* @return bool
	*/
	public function update() {

		//if( $this->find(null, $this->email) ) {
			$data = [
				'name' => $this->name,
				'email' => $this->email,
				'role' => $this->role,
				'status' => $this->status
			];
			
			if( $this->password != '' )
				$data['password'] = sha1($this->password);

			$where = $this->db->quoteInto('id = ?', $this->id);

			if( $this->db->update('user', $data, $where) )
				return true;
		//}

		return false;

	}

	/**
	* Seta o id do usuário
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id do usuário
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome do usuário
	* @name setName()
	* @return void
	*/
	public function setName($name) {
		$this->name = $name;
	}

	/**
	* Retorna o nome do usuário
	* @name getName()
	* @return string
	*/
	public function getName() {
		return $this->name;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getEmail() {
		return $this->email;
	}

	/**
	* Seta a senha do usuário
	* @name setPassword()
	* @return void
	*/
	public function setPassword($password) {
		$this->password = $password;
	}

	/**
	* Retorna a senha do usuário
	* @name getPassword()
	* @return string
	*/
	public function getPassword() {
		return $this->password;
	}

	/**
	* Seta o status do usuário
	* @name setStatus()
	* @return void
	*/
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	* Retorna o status do usuário
	* @name getStatus()
	* @return string
	*/
	public function getStatus() {
		return $this->status;
	}

	/**
	* Seta a permissão do usuário
	* @name setRole()
	* @return void
	*/
	public function setRole($role) {
		$this->role = $role;
	}

	/**
	* Retorna a permissão do usuário
	* @name getRole()
	* @return string
	*/
	public function getRole() {
		return $this->role;
	}

}