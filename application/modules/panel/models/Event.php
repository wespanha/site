<?php

class Panel_Model_Event
{

	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	/**
	* Armazena o id do usuário
	* @name id
	* @access private
	*/
	private $id;
	
	/**
	* Armazena o tipo de evento
	* @name type
	* @access private
	*/
	private $type;

	/**
	* Armazena o ano do evento
	* @name email
	* @access private
	*/
	private $year;

	/**
	* Armazena o regulamento do evento
	* @name regulation
	* @access private
	*/
	private $regulation;

	/**
	* Armazena a imagem de capa
	* @name image
	* @access private
	*/
	private $image;

	/**
	* Armazena a imagem dos patrocinadores
	* @name sponsor
	* @access private
	*/
	private $sponsor;

	/**
	* Armazena a imagem dos patrocinadores responsivo
	* @name sponsor_responsive
	* @access private
	*/
	private $sponsor_responsive;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Método para buscar um usuário especifico
	* @name find()
	* @param $id [id do usuário]
	* @return bool
	*/
	public function find( $id = null, $year = null, $type = null ) {

		$sql = $this->db->select()
					    ->from('event');

		if( !is_null($id) )
			$sql = $sql->where('id = ?', $id);

		if( !is_null($year) )
			$sql = $sql->where('year = ?', $year);

		if( !is_null($type) )
			$sql = $sql->where('type = ?', $type);
		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			$this->id = $result['id'];
			$this->type = $result['type'];
			$this->year = $result['year'];
			$this->regulation = $result['regulation'];
			$this->image = $result['image'];
			$this->sponsor = $result['sponsor'];
			$this->sponsor_responsive = $result['sponsor_responsive'];

			return true;
		}

		return false;
	}

	/**
	* Método para buscar todos os usuários
	* @name findAll()
	* @param $status [filtrar por usuários ativos ou desativados (y, n)]
	* @return array
	*/
	public function findAll( $year = null ) {
		
		$sql = $this->db->select()->from('event')->order(array('year DESC', 'type'));

		if( !is_null($year) )
			$sql = $sql->where('year = ?', $year);
		
		$result = $this->db->fetchAll($sql);
		return ( $result ) ? $result : false;

	}

	/**
	* Método para criar evento
	* @name create()
	* @return bool
	*/
	public function create() {

		if( !$this->find(null, $this->year, $this->type) ) {
			
			$data = [
				'year' 		 => $this->year,
				'type' 		 => $this->type,
				'regulation' => $this->regulation
			];

			mkdir('upload/restaurant/'.$this->type.$this->year, 0777);
			mkdir('upload/sponsor/'.$this->type.$this->year, 0777);

			if( $this->db->insert('event', $data) )
				return true;
		}

		return false;
	}

	/**
	* Método para deletar o evento
	* @name delete()
	* @return bool
	*/
	public function delete($id) {
		
		//busca o caminho da imagem cadastrado
		$sql = $this->db->select()
					    ->from('event')
						->where('id = ?', $id);

		$result = $this->db->fetchRow($sql);

		//exclui a imagem
		unlink($result['image']);
		//eclui o diretorio de restaurantes
		$this->removeDir('upload/restaurant/'.$result['type'].$result['year']);
		$this->removeDir('upload/sponsor/'.$result['type'].$result['year']);
		
		$where = $this->db->quoteInto('id = ?', $id);
		$where2 = $this->db->quoteInto('idEvent = ?', $id);

		$this->db->delete('event', $where);

		return true;
	}

	/**
	* Método para atualizar os registro do evento
	* @name update()
	* @return bool
	*/
	public function update() {

		$data = [
			'year' => $this->year,
			'type' => $this->type,
			'regulation' => $this->regulation
		];

		$where = $this->db->quoteInto('id = ?', $this->id);

		if( $this->db->update('event', $data, $where) )
			return true;
		

		return false;

	}

	/**
	* Método para atualizar a imagem de capa
	* @name updateCape()
	* @return bool
	*/
	public function updateCape() {

		$sql = $this->db->select()
					    ->from('event')
						->where('id = ?', $this->id);

		$result = $this->db->fetchRow($sql);

		//se existe imagem para cadastrar, executa o metodo para gravar
		$pathImage = '';
		if( $this->image != '' ) {

			unlink($result['image']);
			$pathImage = 'upload/cape/'.$this->image;

			$data['image'] = $pathImage;

			$where = $this->db->quoteInto('id = ?', $this->id);

			if( $this->db->update('event', $data, $where) )
				return true;
		}

		return false;

	}

	/**
	* Método para deletar a imagem de capa
	* @name deleteCape()
	* @return bool
	*/
	public function deleteCape() {

		$sql = $this->db->select()
					    ->from('event')
						->where('id = ?', $this->id);

		$result = $this->db->fetchRow($sql);

		unlink($result['image']);
		
		$data['image'] = '';

		$where = $this->db->quoteInto('id = ?', $this->id);

		if( $this->db->update('event', $data, $where) )
			return true;
		

		return false;

	}

	/**
	* Método para buscar a logo dos patrocinadores
	* @name findImageSponsor()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findImageSponsor()
	{
		$sql = $this->db->select()->from('event', array('sponsor', 'sponsor_responsive'))
								  ->where('id = ?', $this->id);

		$result = $this->db->fetchRow($sql);
		return $result;
	}

	/**
	* Método responsável por realizar o upload das imagens dos patrocinadores
	*
	* @name uploadSponsor
	* @return void
	*/
	public function uploadSponsor() {

		$sql = $this->db->select()
					    ->from('event')
						->where('id = ?', $this->id);

		$result = $this->db->fetchRow($sql);

		//se existe imagem para cadastrar, executa o metodo para gravar
		$pathImage = '';
		if( $this->sponsor != '' ) {

			unlink($result['sponsor']);
			$pathImage = 'upload/sponsor/'.$result['type'].$result['year'].'/'.$this->sponsor;

			$data['sponsor'] = $pathImage;

			$where = $this->db->quoteInto('id = ?', $this->id);

			$this->db->update('event', $data, $where);
		}

		if( $this->sponsor_responsive != '' ) {

			unlink($result['sponsor_responsive']);
			$pathImage = 'upload/sponsor/'.$result['type'].$result['year'].'/'.$this->sponsor_responsive;

			$data['sponsor_responsive'] = $pathImage;

			$where = $this->db->quoteInto('id = ?', $this->id);

			$this->db->update('event', $data, $where);
		}

    	return true;
	}

	/**
	* Método para excluir os patrocinadores 
	*
	* @name deleteSponsor()
	* @param $id [id da logo do patrocinador]
	* @return bool
	*/
	public function deleteSponsor( $typeImg ) {

		$sql = $this->db->select()
					    ->from('event')
						->where('id = ?', $this->id);

		$result = $this->db->fetchRow($sql);

		if( $typeImg == 'regular' ) {
			unlink($result['sponsor']);
			$data['sponsor'] = '';
		} else {
			unlink($result['sponsor_responsive']);
			$data['sponsor_responsive'] = '';
		}

		$where = $this->db->quoteInto('id = ?', $this->id);

		if( $this->db->update('event', $data, $where) )
			return true;
		

		return false;
	}

	/**
	* Método para buscar os guias
	* @name findImageRestaurant()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findImageRestaurant()
	{
		$sql = $this->db->select()->from('event', array('id'))
								  ->join('event_restaurant', 'event.id = event_restaurant.idEvent', array('id as idimage','image'))
								  ->where('event.type = ?', $this->type)
								  ->where('event.year = ?', $this->year);

		$result = $this->db->fetchAll($sql);
		return $result;
	}

	/**
	* Método responsável por buscar os dados do restaurante
	* @name fundRestaurant()
	* @param $id [id do restaurante]
	* @return array
	*/
	public function findRestaurant( $id )
	{
		$sql = $this->db->select()->from('event_restaurant')
								  ->where('id = ?', $id);

	  	$result = $this->db->fetchRow($sql);
	  	return $result;
	}

	/**
	* Método responsável por incluir ou alterar os dados de latitude de longitude de um restaurante
	* @name updateRestaurant()
	* @param $data [dados recebidos do formulario com alterações do restaurante]
	* @return bool
	*/
	public function updateRestaurant( $data )
	{
		$where = $this->db->quoteInto('id = ?', $data['id']);

		if( $this->db->update('event_restaurant', $data, $where) )
			return true;

		return false;
	}

	/**
	* Método responsável por realizar o upload das imagens do guia
	*
	* @name uploadRestaurant
	* @return void
	*/
	public function uploadRestaurant() {

		$upload = new Zend_File_Transfer_Adapter_Http();
        $upload->setDestination('upload/restaurant/'.$this->type.$this->year.'/');
        //pega os dados do arquivo
        $files  = $upload->getFileInfo();
        //percorre as imagens
        foreach( $files as $file => $fileInfo ) {
            if ( $upload->isUploaded($file) ) {
                if ( $upload->isValid($file) ) {
                	//se gravou a imagem
                    if ( $upload->receive($file) ) {
                        $info = $upload->getFileInfo($file);

                        $data = [
							'idEvent' 		 => $this->id,
							'image' 		 => 'upload/restaurant/'.$this->type.$this->year.'/'.$fileInfo['name']
						];

						$this->db->insert('event_restaurant', $data);
                 	}
             	}
        	}
    	}

    	return true;
	}

	/**
	* Método para excluir os guias 
	*
	* @name deleteRestaurant()
	* @param $id [id da logo do guia]
	* @return bool
	*/
	public function deleteRestaurant( $id ) {

		$sql = $this->db->select()
					    ->from('event_restaurant')
						->where('id = ?', $id);

		$result = $this->db->fetchRow($sql);

		unlink($result['image']);

		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('event_restaurant', $where) )
			return true;
		

		return false;
	}

	/**
	* Método para excluir a pasta e arquivos dentro do mesmo
	* @name removeDir()
	* @param $dir [caminho do diretorio]
	* @return void
	*/
	public function removeDir($dir){
    
	    if ($dd = opendir($dir)) {
	        while (false !== ($arq = readdir($dd))) {
	            if($arq != "." && $arq != ".."){
	                $path = "$dir/$arq";
	                if(is_dir($path)){
	                    self::excluiDir($path);
	                }elseif(is_file($path)){
	                    unlink($path);
	                }
	            }
	        }
	        closedir($dd);
	    }
	    rmdir($dir);
	}

	/**
	* Seta o id do usuário
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id do usuário
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome do usuário
	* @name setName()
	* @return void
	*/
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	* Retorna o nome do usuário
	* @name getName()
	* @return string
	*/
	public function getYear() {
		return $this->year;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setType($type) {
		$this->type = $type;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getType() {
		return $this->type;
	}

	/**
	* Seta a senha do usuário
	* @name setPassword()
	* @return void
	*/
	public function setRegulation($regulation) {
		$this->regulation = $regulation;
	}

	/**
	* Retorna a senha do usuário
	* @name getPassword()
	* @return string
	*/
	public function getRegulation() {
		return $this->regulation;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setImage($image) {
		$this->image = $image;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getImage() {
		return $this->image;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setSponsor($sponsor) {
		$this->sponsor = $sponsor;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getSponsor() {
		return $this->sponsor;
	}

	/**
	* Seta o email do usuário
	* @name setEmail()
	* @return void
	*/
	public function setSponsorResponsive($sponsor) {
		$this->sponsor_responsive = $sponsor;
	}

	/**
	* Retorna o email do usuário
	* @name getEmail()
	* @return string
	*/
	public function getSponsorResponsive() {
		return $this->sponsor_responsive;
	}

}