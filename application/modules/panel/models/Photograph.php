<?php

class Panel_Model_Photograph
{

	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	/**
	* Armazena o id da galeria
	* @name id
	* @access private
	*/
	private $id;
	
	/**
	* Armazena o nome da galeria
	* @name type
	* @access private
	*/
	private $name;

	/**
	* Armazena a descrição da galeria
	* @name email
	* @access private
	*/
	private $description;

	/**
	* Armazena o id do evento pertencente
	* @name password
	* @access private
	*/
	private $idEvent;

	/**
	* Armazena o id do album do facebook
	* @name password
	* @access private
	*/
	private $idAlbumFacebook;

	/**
	* Armazena o caminho da imagem de capa do album
	* @name password
	* @access private
	*/
	private $imgAlbum;

	/**
	* Armazena o status da galeria
	* @name password
	* @access private
	*/
	private $status;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Método para buscar uma galeria especifica
	* @name find()
	* @param $id [id da galeria]
	* @return bool
	*/
	public function find( $id = null ) {

		$sql = $this->db->select()
					    ->from('photograph')
					    ->where('id = ?', $id);

		
		$result = $this->db->fetchRow($sql);

		if( $result ) {
			$this->id = $result['id'];
			$this->name = $result['name'];
			$this->description = $result['description'];
			$this->idEvent = $result['idEvent'];
			$this->idAlbumFacebook = $result['idAlbumFacebook'];
			$this->imgAlbum = $result['imgAlbum'];
			$this->status = $result['status'];

			return true;
		}

		return false;
	}

	/**
	* Método para buscar todas as galerias de fotos
	* @name findAll()
	* @param $status [filtrar por galeria ativos ou desativados (y, n)]
	* @return array
	*/
	public function findAll( $status = null ) {
		
		$sql = $this->db->select()
						->from('photograph')
						->join('event', 'photograph.idEvent = event.id', array('type','year','regulation'))
						->order(array('id DESC'));

		if( !is_null($status) )
			$sql = $sql->where('status = ?', $status);
		
		$result = $this->db->fetchAll($sql);
		return ( $result ) ? $result : false;

	}

	/**
	* Método para criar a galeria
	* @name create()
	* @return bool
	*/
	public function create() {

		$data = [
			'name' => $this->name,
			'description' => $this->description,
			'idEvent' => $this->idEvent,
			'idAlbumFacebook' => $this->idAlbumFacebook,
			'imgAlbum' => $this->imgAlbum,
			'status' => $this->status
		];

		if( $this->db->insert('photograph', $data) )
			return true;
		

		return false;
	}

	/**
	* Método para deletar a galeria
	* @name delete()
	* @return bool
	*/
	public function delete($id) {

		$where = $this->db->quoteInto('id = ?', $id);

		if( $this->db->delete('photograph', $where) )
			return true;

		return false;
	}

	/**
	* Método para atualizar os registros da galeria
	* @name update()
	* @return bool
	*/
	public function update() {

		$data = [
			'name' => $this->name,
			'description' => $this->description,
			'idEvent' => $this->idEvent,
			'idAlbumFacebook' => $this->idAlbumFacebook,
			'imgAlbum' => $this->imgAlbum,
			'status' => $this->status
		];

		$where = $this->db->quoteInto('id = ?', $this->id);

		if( $this->db->update('photograph', $data, $where) )
			return true;

		return false;

	}

	/**
	* Seta o id da galeria
	* @name setId()
	* @return void
	*/
	public function setId($id) {
		$this->id = $id;
	}

	/**
	* Retorna o id da galeria
	* @name getId()
	* @return int
	*/
	public function getId() {
		return $this->id;
	}

	/**
	* Seta o nome da galeria de foto
	* @name setName()
	* @return void
	*/
	public function setName($name) {
		$this->name = $name;
	}

	/**
	* Retorna o nome da galeria
	* @name getName()
	* @return string
	*/
	public function getName() {
		return $this->name;
	}

	/**
	* Seta a descrição do album
	* @name setEmail()
	* @return void
	*/
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	* Retorna a descrição do album
	* @name getEmail()
	* @return string
	*/
	public function getDescription() {
		return $this->description;
	}

	/**
	* Seta o id do evento que sera vinculado o album
	* @name setPassword()
	* @return void
	*/
	public function setIdEvent($event) {
		$this->idEvent = $event;
	}

	/**
	* Retorna o id do evento que sera vinculado
	* @name getPassword()
	* @return string
	*/
	public function getIdEvent() {
		return $this->idEvent;
	}

	/**
	* Seta o id do album no facebook
	* @name setPassword()
	* @return void
	*/
	public function setIdAlbumFacebook($idFacebook) {
		$this->idAlbumFacebook = $idFacebook;
	}

	/**
	* Retorna o id do album no facebook
	* @name getPassword()
	* @return string
	*/
	public function getIdAlbumFacebook() {
		return $this->idAlbumFacebook;
	}

	/**
	* Seta o caminho da imagem de capa do album
	* @name setPassword()
	* @return void
	*/
	public function setImgAlbum($imgAlbum) {
		$this->imgAlbum = $imgAlbum;
	}

	/**
	* Retorna o caminho da imagem de capa do album
	* @name getPassword()
	* @return string
	*/
	public function getImgAlbum() {
		return $this->imgAlbum;
	}

	/**
	* Seta o status da galeria
	* @name setPassword()
	* @return void
	*/
	public function setStatus($status) {
		$this->status = $status;
	}

	/**
	* Retorna o status da galeria
	* @name getPassword()
	* @return string
	*/
	public function getStatus() {
		return $this->status;
	}

}