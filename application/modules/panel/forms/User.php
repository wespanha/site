<?php

class Panel_Form_User extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$name = new Zend_Form_Element_Text('name');
		$name->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Nome')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					'placeholder' => 'Nome Completo',
			 					'required' => ''));
		$this->addElement($name);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('E-mail')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 	 				 'placeholder' => 'E-mail',
			 					 'required' => ''));
		$this->addElement($email);

		$status = new Zend_Form_Element_Select('status');
		$status->removeDecorator('Label')
		   	   ->removeDecorator('HtmlTag')
		   	   ->setLabel('Status')
		   	   ->setRequired()
			   ->setErrorMessages(array('Campo obrigatório'))
			   ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 				  'required' => ''))
			   ->addMultiOptions(array('y' => 'Ativo',
			   						   'n' => 'Desativado'));
		$this->addElement($status);

		$role = new Zend_Form_Element_Select('role');
		$role->removeDecorator('Label')
		   	 ->removeDecorator('HtmlTag')
		   	 ->setLabel('Permissão')
		   	 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'required' => ''))
			 ->addMultiOptions(array('administrator' => 'Administrador',
			 						 'publisher'     => 'Editor'));
		$this->addElement($role);

		$password = new Zend_Form_Element_Password('password');
		$password->removeDecorator('Label')
				 ->removeDecorator('HtmlTag')
				 ->setLabel('Senha')
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				 					'placeholder' => 'Senha'));
		$this->addElement($password);


	}

}