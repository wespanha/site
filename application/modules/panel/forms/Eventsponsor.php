<?php

class Panel_Form_Eventsponsor extends Zend_Form 
{

	public function __construct($type, $year) {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$image = new Zend_Form_Element_File('image');
		$image->removeDecorator('Label')
			  ->removeDecorator('HtmlTag')
			  ->setLabel('Carregar imagem dos patrocinadores')
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12'))
			  ->setDestination('upload/sponsor/'.$type.$year.'/');
		$this->addElement($image);

		$image_responsive = new Zend_Form_Element_File('image_responsive');
		$image_responsive->removeDecorator('Label')
			  			 ->removeDecorator('HtmlTag')
			  			 ->setLabel('Carregar imagem dos patrocinadores | Responsivo')
			  			 ->setErrorMessages(array('Campo obrigatório'))
			  			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12'))
			  			 ->setDestination('upload/sponsor/'.$type.$year.'/');
		$this->addElement($image_responsive);

	}

}