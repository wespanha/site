<?php

class Panel_Form_Event extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$type = new Zend_Form_Element_Select('type');
		$type->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Tipo de evento')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					'required' => ''))
			 ->addMultiOptions(array('culinaria-a-ceu-aberto'   => 'Culinária a céu aberto',
			 						 'concurso' 				=> 'Concurso',
			 						 'rota-do-sabor'	   		=> 'Rota do Sabor'));
		$this->addElement($type);

		$year = new Zend_Form_Element_Text('year');
		$year->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('Ano')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					 'required' => ''));
		$this->addElement($year);

		$regulation = new Zend_Form_Element_Textarea('regulation');
		$regulation->removeDecorator('Label')
			   	   ->removeDecorator('HtmlTag')
			   	   ->setLabel('Regulamento')
				   ->setAttribs(array('class' => 'form-control col-md-7 col-xs-12',
				   					  'id'    => 'textarea'));
		$this->addElement($regulation);


	}

}