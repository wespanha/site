<?php

class Panel_Form_Restaurantdetail extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$name = new Zend_Form_Element_Text('name');
		$name->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('Nome')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class' 	=> 'form-control col-md-7 col-xs-12',
			 					 'required' => ''));
		$this->addElement($name);

		$latitude = new Zend_Form_Element_Text('latitude');
		$latitude->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('Latitude')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class' 	=> 'form-control col-md-7 col-xs-12',
			 					 'required' => ''));
		$this->addElement($latitude);

		$longitude = new Zend_Form_Element_Text('longitude');
		$longitude->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('Longitude')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class' 	=> 'form-control col-md-7 col-xs-12',
			 					 'required' => ''));
		$this->addElement($longitude);


	}

}