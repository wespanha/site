<?php

class Panel_Form_Photograph extends Zend_Form 
{

	public function __construct($events) {

		$typeEvent = [
			'culinary' => 'Culinária a céu aberto',
			'competition' => 'Concurso',
			'route' => 'Rota do Sabor'
		];

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$name = new Zend_Form_Element_Text('name');
		$name->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Nome da galeria')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					'required' => ''));
		$this->addElement($name);

		$description = new Zend_Form_Element_Text('description');
		$description->removeDecorator('Label')
			   		->removeDecorator('HtmlTag')
			   		->setLabel('Descrição')
					->setErrorMessages(array('Campo obrigatório'))
					->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12'));
		$this->addElement($description);

		$idEvent = new Zend_Form_Element_Select('idEvent');
		$idEvent->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Selecione o evento')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					'required' => ''));

		foreach ($events as $key => $value) {
			$idEvent->addMultiOption($value['id'], $typeEvent[$value['type']].' / '.$value['year']);
		}

		$this->addElement($idEvent);

		$idAlbumFacebook = new Zend_Form_Element_Text('idAlbumFacebook');
		$idAlbumFacebook->removeDecorator('Label')
				   		->removeDecorator('HtmlTag')
				   		->setLabel('Id do Album no Facebook')
				   		->setRequired()
						->setErrorMessages(array('Campo obrigatório'))
						->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
						 				   'required' => ''));
		$this->addElement($idAlbumFacebook);

		$imgAlbum = new Zend_Form_Element_Text('imgAlbum');
		$imgAlbum->removeDecorator('Label')
		   		 ->removeDecorator('HtmlTag')
		   		 ->setLabel('Foto de capa do album')
		   		 ->setRequired()
				 ->setErrorMessages(array('Campo obrigatório'))
				 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
				     				'required' => ''));
		$this->addElement($imgAlbum);

		$status = new Zend_Form_Element_Select('status');
		$status->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Status da galeria de fotos')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12',
			 					'required' => ''))
			 ->addMultiOptions(array('y'    => 'Ativo',
			 						 'n' => 'Desativado'));
		$this->addElement($status);


	}

}