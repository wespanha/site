<?php

class Panel_Form_Eventcape extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$image = new Zend_Form_Element_File('image');
		$image->removeDecorator('Label')
			  ->removeDecorator('HtmlTag')
			  ->setLabel('Carregar imagem de capa')
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control col-md-7 col-xs-12'))
			  ->setDestination('upload/cape/');
		$this->addElement($image);

	}

}