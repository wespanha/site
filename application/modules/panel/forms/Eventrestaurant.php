<?php

class Panel_Form_Eventrestaurant extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$year = new Zend_Form_Element_Hidden('year');
		$year->removeDecorator('Label')
		     ->removeDecorator('HtmlTag');
		$this->addElement($year);

		$type = new Zend_Form_Element_Hidden('type');
		$type->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($type);

		$this->addElement('file', 'images',
		    array(
		        'label'         => 'Imagem do guia',
		        'valueDisabled' => true,
		        'isArray'       => true,
		        'multiple'      => true,
		        'class'			=> 'form-control col-md-7 col-xs-12',
		        'setDestination'=> 'upload/guide/',
		        'validators'    => array(
		            ['Extension', false, 'jpg,png,gif'],
		        ),
		    )
		);

	}

}