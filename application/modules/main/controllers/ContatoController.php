<?php

class Main_ContatoController extends Zend_Controller_Action
{
	
	private $model_front;

	private $sessionYear;

    private $form_contact;

    private $layout;

    public function init()
    {	
        $this->layout = Zend_Layout::getMvcInstance();

    	$this->model_front = new Main_Model_Front();
        $this->form_contact = new Main_Form_Contact();

        $this->sessionYear = $this->model_front->findYear();
        Zend_Registry::set('year', $this->sessionYear);
        
    }

    public function indexAction()
    {   
        $this->layout->assign('title', 'Contato');

        if( $this->getRequest()->isPost() ) {
            if( $this->form_contact->isValid($this->getRequest()->getPost()) ) {

                $config = array('ssl' => 'ssl',
                                'auth' => 'login',
                                'username' => 'desenvolvimento@wespanha.com.br',
                                'port' => '465',
                                'password' => 'devwespanha');

                $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

                $html  = '<p><strong>Nome: </strong>'.$this->form_contact->getValue('name').'</p>';
                $html .= '<p><strong>E-mail: </strong>'.$this->form_contact->getValue('email').'</p>';
                $html .= '<p><strong>Assunto: </strong>'.$this->form_contact->getValue('subject').'</p>';
                $html .= '<p><strong>Mensagem: </strong>'.$this->form_contact->getValue('message').'</p>';

                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHtml($html);
                $mail->setFrom('desenvolvimento@wespanha.com.br', 'Contato Site');
                $mail->addTo('priscila@wespanha.com.br');
                $mail->setSubject('Contato através do site Festival Gourmet de Varginha');

                if( $mail->send($transport) ) {
                    //limpa os valores do formulário
                    $this->form_contact->name->setValue('');
                    $this->form_contact->email->setValue('');
                    $this->form_contact->subject->setValue('');
                    $this->form_contact->message->setValue('');
                    
                    $this->view->assign('status', true);
                    $this->view->assign('message', 'O e-mail foi enviado com sucesso.');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível enviar o e-mail, favor tente mais tarde.');
                }
            }
        }

        $this->view->assign('form', $this->form_contact);
    }

}