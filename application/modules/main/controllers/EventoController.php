<?php

class Main_EventoController extends Zend_Controller_Action
{

    /**
    * Armazena a instancia do model do site
    * @name model_front
    * @access private
    */
	private $model_front;

    /**
    * Armazena a instancia do model do site
    * @name event_name
    * @access private
    */
    private $event_name;

    /**
    * Armazena a instancia do model do site
    * @name type
    * @access private
    */
    private $type;

    /**
    * Armazena a instancia do model do site
    * @name year
    * @access private
    */
    private $year;

    private $layout;

    public function init()
    {	
        $this->layout = Zend_Layout::getMvcInstance();

    	$this->model_front = new Main_Model_Front();
        Zend_Registry::set('year', $this->model_front->findYear());
        //nomes do evento por type
        $this->event_name = [
            'culinaria-a-ceu-aberto'    => 'Culinária a céu aberto (Shopping)',
            'concurso'                  => 'Concurso',
            'rota-do-sabor'             => 'Rota do Sabor'
        ];

        if( $this->getRequest()->getActionName() == 'edicao' ) {

            if( ($this->getRequest()->getParam('categoria')) )
                $this->type = $this->getRequest()->getParam('categoria');
            else 
                $this->redirect('/');

        } else {

            if( $this->getRequest()->getParams() ) {
            $params = $this->getRequest()->getParams();
                foreach ($params as $name => $value) {
                    if( $name == 'culinaria-a-ceu-aberto' || $name == 'concurso' || $name == 'rota-do-sabor' ) {
                        $name_event = $name;
                        $year = $value;
                    }
                }
                $this->type = $name_event;
                $this->year = $year;
            }
            
        }
    }

    public function categoriaAction()
    {   
        //imagem de capa
        $this->view->assign('imgCape', $this->model_front->findImageCape( $this->type, $this->year ));
        //imagem dos patrocinadores
        $this->view->assign('imgSponsor', $this->model_front->findImageSponsor( $this->type, $this->year ));
        //imagem dos guias
        $this->view->assign('imgRestaurant', $this->model_front->findImageRestaurant( $this->type, $this->year ));
        //regulamento
        $this->view->assign('regulation', $this->model_front->findRegulation( $this->type, $this->year ));
        //listagem de album
        $photographs = $this->model_front->findAllPhotograph( $this->type, $this->year );
        $this->view->assign('photographs', $photographs);
        
        if(explode('@',$this->year)) {
            $year = explode('@',$this->year);
            $this->year = $year[0];
        }

        //assina para view os parametros obrigatorios
        $this->view->assign('type', $this->type);
        $this->view->assign('year', $this->year);
        $this->view->assign('event_name', $this->event_name[$this->type]);

        //seta o titulo da página
        $title['culinary'] = 'Culinária a céu aberto (Shopping)';
        $title['competition'] = 'Concurso';
        $title['route'] = 'Rota do Sabor';

        $this->layout->assign('title', $title[$this->type]);

    }

    public function fotosAction()
    {
        //verifica se existe idAlbumFacebook, caso exista exibe as fotos
        if( $this->getRequest()->getParam('idAlbumFacebook') )
            $this->view->assign('idAlbumFacebook', $this->getRequest()->getParam('idAlbumFacebook'));
        else
            $this->redirect('/');

        $params = $this->getRequest()->getParams();
        foreach ($params as $name => $value) {
            if( $name == 'culinaria-a-ceu-aberto' || $name == 'concurso' || $name == 'rota-do-sabor' ) {
                $this->view->assign('event_name', $this->event_name[$name]);
                $this->view->assign('imgCape', $this->model_front->findImageCape( $name, $value ));
            }
        }
    }

    public function edicaoAction()
    {
        $this->layout->assign('title', 'Edições Anteriores');

        //lista os anos cadastrados
        $this->view->assign('years', $this->model_front->findYearEdition( $this->type ));
    
        //assina para view os parametros obrigatorios
        $this->view->assign('type', $this->type);
        $this->view->assign('event_name', $this->event_name[$this->type]);
    }

    public function restauranteAction()
    {
        //verifica se foi setado o id do restaurante
        if( $this->getParam('id') ) {
            //busca os dados do restaurante
            $restaurant = $this->model_front->findRestaurant( $this->getParam('id') );
            $this->view->assign('restaurant', $restaurant);
        }
        
    }

}

