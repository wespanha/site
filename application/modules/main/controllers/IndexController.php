<?php

class Main_IndexController extends Zend_Controller_Action
{

	private $model_front;

	private $sessionYear;

    private $form_partner;

    public function init()
    {	

        $this->model_front = new Main_Model_Front();
        $this->form_partner = new Main_Form_Partner();

        $this->sessionYear = $this->model_front->findYear();
        Zend_Registry::set('year', $this->sessionYear);

    }

    public function indexAction()
    { 

        if( $this->getRequest()->isPost() ) {
            if( $this->form_partner->isValid($this->getRequest()->getPost()) ) {

                $config = array('ssl' => 'ssl',
                                'auth' => 'login',
                                'username' => 'desenvolvimento@wespanha.com.br',
                                'port' => '465',
                                'password' => 'devwespanha');

                $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

                $html = '<p><strong>Quer ser parceiro deste evento?</strong></p>';
                $html .= '<p><strong>Nome: </strong>'.utf8_decode($this->form_partner->getValue('name')).'</p>';
                $html .= '<p><strong>E-mail: </strong>'.utf8_decode($this->form_partner->getValue('email')).'</p>';

                $mail = new Zend_Mail('UTF-8');
                $mail->setBodyHtml($html);
                $mail->setFrom('desenvolvimento@wespanha.com.br', 'Contato Site');
                $mail->addTo('priscila@wespanha.com.br');
                $mail->setSubject('Contato através do site Festival Gourmet de Varginha | Parceiro do evento');

                if( $mail->send($transport) ) {
                    //limpa os valores do formulário
                    $this->form_partner->name->setValue('');
                    $this->form_partner->email->setValue('');

                    $this->view->assign('status', true);
                    $this->view->assign('message', 'Mensagem enviada com sucesso!');
                } else {
                    $this->view->assign('status', false);
                    $this->view->assign('message', 'Não foi possível enviar sua mensagem. Favor, tente mais tarde.');
                }
            }
        }

        $photographs = $this->model_front->findCurrentPhotograph( $this->sessionYear );
        $this->view->assign('photographs', $photographs);

        $this->view->assign('year', $this->sessionYear);
        $this->view->assign('form', $this->form_partner);
    }

}

