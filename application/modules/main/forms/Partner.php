<?php

class Main_Form_Partner extends Zend_Form 
{

	public function init() {

		$name = new Zend_Form_Element_Text('name');
		$name->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Nome')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control',
			 					'required' => '',
			 					'placeholder' => 'Nome da empresa'));
		$this->addElement($name);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('E-mail')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control',
			 					 'required' => '',
			 					 'placeholder' => 'E-mail'));
		$this->addElement($email);

	}

}