<?php

class Main_Form_Contact extends Zend_Form 
{

	public function init() {

		$id = new Zend_Form_Element_Hidden('id');
		$id->removeDecorator('Label')
		   ->removeDecorator('HtmlTag');
		$this->addElement($id);

		$name = new Zend_Form_Element_Text('name');
		$name->removeDecorator('Label')
	   		 ->removeDecorator('HtmlTag')
	   		 ->setLabel('Nome')
	   		 ->setRequired()
			 ->setErrorMessages(array('Campo obrigatório'))
			 ->setAttribs(array('class'       => 'form-control',
			 					'required' => '',
			 					'placeholder' => 'Nome'));
		$this->addElement($name);

		$subject = new Zend_Form_Element_Text('subject');
		$subject->removeDecorator('Label')
	   		 	->removeDecorator('HtmlTag')
	   		 	->setLabel('Assunto')
	   		 	->setRequired()
			 	->setErrorMessages(array('Campo obrigatório'))
			 	->setAttribs(array('class'       => 'form-control',
			 					   'required' => '',
			 					   'placeholder' => 'Assunto'));
		$this->addElement($subject);

		$email = new Zend_Form_Element_Text('email');
		$email->removeDecorator('Label')
	   		  ->removeDecorator('HtmlTag')
	   		  ->setLabel('E-mail')
	   		  ->setRequired()
			  ->setErrorMessages(array('Campo obrigatório'))
			  ->setAttribs(array('class'       => 'form-control',
			 					 'required' => '',
			 					 'placeholder' => 'E-mail'));
		$this->addElement($email);

		$message = new Zend_Form_Element_Textarea('message');
		$message->removeDecorator('Label')
			   	->removeDecorator('HtmlTag')
			   	->setLabel('Mensagem')
			   	->setRequired()
				->setErrorMessages(array('Campo obrigatório'))
				->setAttribs(array('class' => 'form-control',
				   				   'id'    => 'textarea',
				   				   'cols'  => '30',
				   				   'rows'  => '10',
				   				   'placeholder' => 'Mensagem'));
		$this->addElement($message);


	}

}