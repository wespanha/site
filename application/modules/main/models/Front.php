<?php

class Main_Model_Front
{

	/**
	* Armazena conexão com o Banco de Dados
	* @name db
	* @access private
	*/
	private $db;

	/**
	* Método construtor para instânciar a conexão com banco de dados
	* @name _construct()
	* @return void
	*/
	public function __construct() {
		$this->db = Zend_Registry::get('db');
	}

	/**
	* Método para buscar um usuário especifico
	* @name find()
	* @param $id [id do usuário]
	* @return bool
	*/
	public function findYear() {

		$sql = $this->db->select()->from('event', array('year'))->order('year DESC');

		$result = $this->db->fetchRow($sql);
		return $result['year'];
		
	}

	/**
	* Método para buscar todos os albuns de fotos de um evento
	* @name findAllPhotograph()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findAllPhotograph( $type, $year )
	{
		$sql = $this->db->select()->from('event')
								  ->join('photograph', 'photograph.idEvent = event.id')
								  ->where('type = ?', $type)
								  ->where('year = ?', $year)
								  ->order('photograph.id DESC');

		return $this->db->fetchAll($sql);
	}

	/**
	* Método para buscar a imagem de capa do evento
	* @name findImageCover()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findImageCape( $type, $year )
	{
		$sql = $this->db->select()->from('event', array('image'))
								  ->where('type = ?', $type)
								  ->where('year = ?', $year);

		$result = $this->db->fetchRow($sql);
		return $result['image'];
	}

	/**
	* Método para buscar a logo dos patrocinadores
	* @name findImageSponsor()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findImageSponsor( $type, $year )
	{
		$sql = $this->db->select()->from('event', array('sponsor', 'sponsor_responsive'))
								  ->where('event.type = ?', $type)
								  ->where('event.year = ?', $year);

		$result = $this->db->fetchRow($sql);
		return $result;
	}

	/**
	* Método para buscar as imagens dos restaurantes
	* @name findImageRestaurant()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findImageRestaurant( $type, $year )
	{
		$sql = $this->db->select()->from('event', array('id'))
								  ->join('event_restaurant', 'event.id = event_restaurant.idEvent', array('image', 'id as idrestaurant'))
								  ->where('event.type = ?', $type)
								  ->where('event.year = ?', $year)
								  ->order('event_restaurant.name ASC');

		$result = $this->db->fetchAll($sql);
		return $result;
	}

	/**
	* Método para buscar os regulamento do evento
	* @name findRegulation()
	* @param $type [tipo de evento]
	* @param $year [ano do evento]
	* @return array
	*/
	public function findRegulation( $type, $year )
	{
		$sql = $this->db->select()->from('event', array('regulation'))
								  ->where('type = ?', $type)
								  ->where('year = ?', $year);

		$result = $this->db->fetchRow($sql);
		return $result['regulation'];
	}

	/**
	* Método para buscar os anos de um evento
	* @name findYearEdition()
	* @param $type [tipo de evento]
	* @return array
	*/
	public function findYearEdition( $type ) {
		$sql = $this->db->select()->from('event')
								  ->where('type = ?', $type)
								  ->where('year <> ?', $this->findYear())
								  ->order('year DESC');

		$result = $this->db->fetchAll($sql);
		return $result;
	}

	/**
	* Método para remover todos os caracteres especiais e espaços de um texto
	* @name format_text()
	* @param $string [texto para ser formatado]
	* @return string
	*/
	public function format_text( $string ) {

	    $string = preg_replace( '/[`^~\'",.{}ºª;:!@#$¨&*()_+=§£¢¬]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );;
	    return str_replace(' ', '-', $string);
	}

	/**
	* Método responsável por buscar as informações de um restaurante
	* @name fundRestaurant()
	* @param $id [id do restaurante]
	* @return array
	*/
	public function findRestaurant( $id )
	{
		$sql = $this->db->select()->from('event_restaurant')
								  ->where('id = ?', $id);

	  	$result = $this->db->fetchRow($sql);
	  	return $result;
	}

	/**
	* Método responsável por listar os ultimos albuns de fotos do evento atual
	* @name findCurrentPhotograph()
	* @param $year [ano do ultimo evento cadastrado]
	* @return array
	*/
	public function findCurrentPhotograph( $year )
	{
		$sql = $this->db->select()->from('photograph')
								  ->join('event', 'event.id = photograph.idEvent', array('event.id as idEvent', 'event.year as year', 'event.type as type'))
								  ->where('event.year = ?', $year)
								  ->limit(3)
								  ->order('photograph.id DESC');

	  	$result = $this->db->fetchAll($sql);
	  	return $result;
	}

}
