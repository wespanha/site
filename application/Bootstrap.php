<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * Inicializa o banco de dados. Somente necessario se deseja salvar a conexao no registro
     * @name _initDB
     * @return void
     */
    public function _initDB() {
        $db = $this->getPluginResource('db')->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }

    /**
     * Inicializa o Namespace do plugin Acl
     * @name _initAutoLoader
     * @return void
     */
    protected function _initAutoloader() {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('Plugins');
    }

    /**
     * Inicializa o Plugin App/Plugin/Acl.php
     * @name _initPlugins
     * @return void
     */
    protected function _initPlugins() {
        $bootstrap = $this->getApplication();
        if ($bootstrap instanceof Zend_Application) {
            $bootstrap = $this;
        }
        $bootstrap->bootstrap('FrontController');
        $front = $bootstrap->getResource('FrontController');

        $front->registerPlugin(new Plugins_Auth_Acl());

    }

    /**
     * Função para carregar automaticamente o diretorio de layout's para cada modulo
     * @name _initLayout
     * @return void
     */
    public function _initLayout() {
        $request_uri = explode('/', $_SERVER['REQUEST_URI']);
        $modules = array('main','panel');
        $layout = "main";

        foreach($modules as $key => $value) {
            (in_array($value, $request_uri)) ? $layout = $value : '';
        }

        $option = array(
            'layout'        => 'layout',
            'layoutPath' => APPLICATION_PATH . "/modules/". $layout . "/layouts"
        );

        Zend_Layout::startMvc($option);
    }
    
    /**
     * Função responsável por conectar a api do sistema
     * @name _initConexaoWs
     * @return void
     */
    public function _initConexaoWs() {
        
    }

    /**
     * Inicializa com banco de dados Sql
     * @name _initDatabase
     * @return void
     */
    protected function _initDatabase() {
        //@var $resource Zend_Application_Resource_Multidb
        /*$resource = $this->getPluginResource('multidb');*/
        // inicia as conexões aos bancos de dados
        /*$resource->init();
        Zend_Registry::set('mssql', $resource->getDb('mssql'));*/
    }

    
    public function _initAcl() {


        // ---------------------------- PAPÉIS ---------------------------- //

        
        $acl = new Zend_Acl();
        $acl->addRole('guest')
            ->addRole('publisher', 'guest')
            ->addRole('administrator', 'publisher');


        // ---------------------------- SITE ---------------------------- //
        

        //site página inicial
        $acl->addResource('main:index:index');

        //site página de erro
        $acl->addResource('main:error:error');
        $acl->addResource('main:error:denied');

        //site página de evento
        $acl->addResource('main:evento:categoria');
        $acl->addResource('main:evento:fotos');
        $acl->addResource('main:evento:edicao');
        $acl->addResource('main:evento:restaurante');

        //site página de contato
        $acl->addResource('main:contato:index');
        
        //site página de inscrição
        $acl->addResource('main:inscription:index');

        
        /********* Atribuindo permissões para os papéis ***********/
        

        //site página inicial
        $acl->allow('guest', 'main:index:index');

        //site página de erro
        $acl->allow('guest', 'main:error:error');
        $acl->allow('guest', 'main:error:denied');

        //site página de evento
        $acl->allow('guest', 'main:evento:categoria');
        $acl->allow('guest', 'main:evento:fotos');
        $acl->allow('guest', 'main:evento:edicao');
        $acl->allow('guest', 'main:evento:restaurante');

        //site página de contato
        $acl->allow('guest', 'main:contato:index');

        //site página de inscrição
        $acl->allow('guest', 'main:inscription:index');


        // ---------------------------- PAINEL ---------------------------- //


        //painel login
        $acl->addResource('panel:index:index');
        $acl->addResource('panel:index:logout');

        //painel página de erro
        $acl->addResource('panel:error:error');

        //painel dashboard
        $acl->addResource('panel:dashboard:index');

        //painel usuário
        $acl->addResource('panel:user:index');
        $acl->addResource('panel:user:create');
        $acl->addResource('panel:user:update');
        $acl->addResource('panel:user:delete');

        //painel evento
        $acl->addResource('panel:event:index');
        $acl->addResource('panel:event:create');
        $acl->addResource('panel:event:update');
        $acl->addResource('panel:event:delete');
        $acl->addResource('panel:event:cape');
        $acl->addResource('panel:event:sponsor');
        $acl->addResource('panel:event:restaurant');
        $acl->addResource('panel:event:detailrestaurant');

        //panel album de fotos
        $acl->addResource('panel:photograph:index');
        $acl->addResource('panel:photograph:create');
        $acl->addResource('panel:photograph:update');
        $acl->addResource('panel:photograph:delete');


        /*********** Atribuindo permissões para os papéis ***********/


        //painel login e logout
        $acl->allow('guest', 'panel:index:index');
        $acl->allow('guest', 'panel:index:logout');

        //painel dashboard
        $acl->allow('publisher', 'panel:dashboard:index');

        //painel usuário
        $acl->allow('administrator', 'panel:user:index');
        $acl->allow('administrator', 'panel:user:create');
        $acl->allow('administrator', 'panel:user:update');
        $acl->allow('administrator', 'panel:user:delete');

        //painel evento
        $acl->allow('administrator', 'panel:event:index');
        $acl->allow('administrator', 'panel:event:create');
        $acl->allow('administrator', 'panel:event:update');
        $acl->allow('administrator', 'panel:event:delete');
        $acl->allow('administrator', 'panel:event:cape');
        $acl->allow('administrator', 'panel:event:sponsor');
        $acl->allow('administrator', 'panel:event:restaurant');
        $acl->allow('administrator', 'panel:event:detailrestaurant');

        //painel album de fotos
        $acl->allow('publisher', 'panel:photograph:index');
        $acl->allow('publisher', 'panel:photograph:create');
        $acl->allow('publisher', 'panel:photograph:update');
        $acl->allow('publisher', 'panel:photograph:delete');


        // ---------------------------------------------------------------- //

        
        //registra as permissões
        Zend_Registry::set('acl', $acl);
    }
}

